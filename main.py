

# -----------------------------------------------------------------------------
#
# DESCRIPTION
#
# THIS SCRIPT PERFORMS VARIANT ANALYSIS FILTERING ACCORDING PHENODB ANALYSIS MODULE
#
# MORE DETAILS MIGHT BE FOUND AT https://mendeliangenomics.org/
#
# Sobreira N, Schiettecatte F, Boehm C, Valle D, Hamosh A. New tools
# for Mendelian disease gene identification: PhenoDB variant analysis
# module; and GeneMatcher, a web-based tool for linking investigators
# with an interest in the same gene. Hum Mutat. 2015 Apr;36(4):425-31.
# doi: 10.1002/humu.22769. PMID: 25684268; PMCID: PMC4820250.
#
# TEST DATASET NOT REAL GENOMIC DATA FROM A REAL SUBJECT
#


# -----------------------------------------------------------------------------
#
# IMPORTS
#

import sys
import os


# -----------------------------------------------------------------------------
#
# CONSTANTS
#

# AFFECTED STATUS ACCORDING TO GATK PED
AFFECTED_STATUS_AFFECTED = 2
AFFECTED_STATUS_UNAFFECTED = 1
AFFECTED_STATUS_MISSING = 0

# SEX ID ACCORDING TO GATK PED
SEX_MALE = 1
SEX_FEMALE = 2
SEX_UNKNOWN = 0

# FAMILY_MEMBER_ID ACCORDING TO PHENODB
# REMAINING SAMPLES SHOULD BE NUMBERED FROM 4 TO N, REGARDLESS THE PRESENCE OF MOTHER OR FATHER
FAMILY_MEMBER_PATIENT = 1
FAMILY_MEMBER_MOTHER = 2
FAMILY_MEMBER_FATHER = 3

# THESE ARE THE COLUMNS WE ARE GOING TO USE TO FILTER VARIANTS
# THESE COLUMNS ARE FOR THE THREE EXAMPLE FILES
# THE SCRIPT WILL IDENTIFY THE COLUMNS BASED ON THE HEADER THOUGH
COL_CHROMOSOME = 0
COL_START_POSITION = 1
COL_REF_ALLELE = 3
COL_ALT_ALLELE = 4
COL_GENOTYPE = 5
COL_GENE_LOCATION = 11
COL_GENE_NAME = 12
COL_EXON_FUNC = 13
COLS_MAF = [77, 78, 79, 80, 81, 82, 83, 84]

# THESE ARE THE MAIN ANALYSIS AVAILABLE ON PHENODB
# SO FAR ANALYSIS_TYPE_AD_V IS THE ONLY IMPLEMENTED AND WORKING

# ANALYSIS_TYPE_AR_CH – ALL VARIANTS APPEARING AT LEAST TWICE IN A GENE FOUND WITH THE SAME CONFIGURATION IN ALL AFFECTED SAMPLES AND NOT FOUND WITH THE SAME PATTERN IN ANY UNAFFECTED SAMPLE
# ANALYSIS_TYPE_AR_H – ALL VARIANTS FOUND ON ALL AFFECTED INDIVIDUALS AS HOMOZYGOUS AND NOT FOUND ON UNAFFECTED INDIVIDUALS AS HOMOZYGOUS
# ANALYSIS_TYPE_AD_NM – ALL VARIANTS FOUND ON PROBAND NOT PRESENT ON PARENTS (ONLY APPLIED FOR TRIO ANALYSIS)
# ANALYSIS_TYPE_AD_IM – ALL VARIANTS FOUND ON AFFECTED SAMPLES AND NOT FOUND ON UNAFFECTED SAMPLES (APPLIED FOR MULTIPLES AFFECTED INDIVIDUALS ON THE FAMILY)
# ANALYSIS_TYPE_AD_V – ALL HETEROZYGOUS VARIANTS PRESENT ON PROBAND
ANALYSIS_TYPE_AR_CH = 0
ANALYSIS_TYPE_AR_H = 1
ANALYSIS_TYPE_AD_NM = 2
ANALYSIS_TYPE_AD_IM = 3
ANALYSIS_TYPE_AD_V = 4

# REFGENE_GENE_LOCATION_OPTIONS ARE FROM ANNOVAR REFGENE ANNOTATION
REFGENE_GENE_LOCATION_OPTIONS = ("downstream", "exonic", "exonic;splicing", "intergenic", "intronic", "ncRNA_exonic",
                                 "ncRNA_intronic", "ncRNA_splicing", "ncRNA_UTR3", "ncRNA_UTR5", "splicing", "upstream",
                                 "upstream;downstream", "UTR3", "UTR5", "UTR5;UTR3")

# PHENODB STANDARD ANALYSIS DOES NOT FILTER BASED ON REFGENE EXON FUNCTION ANNOTATION
# AS STANDARD ANALYSIS VARIANTS ARE EXCLUDED IF SYNONYMOUS SNV EXCEPT IN REFGENE_GENE_LOCATION EXONIC;SPLICING REGION
REFGENE_EXON_FUNCTION_OPTIONS = ("frameshift deletion", "frameshift insertion", "frameshift substitution",
                                 "nonframeshift deletion", "nonframeshift insertion", "nonframeshift substitution",
                                 "nonsynonymous SNV", "RefgeneExonFunction", "stopgain SNV", "stoploss SNV",
                                 "synonymous SNV", "unknown")

# THIS IS THE STANDARD SELECTION THAT WAS IMPLEMENTED FOR ALL ANALYSIS
# PHENODB ALLOWS USERS TO FREELY SELECT THE DESIRED COMBINATION OF OPTIONS
REFGENE_GENE_LOCATION_STANDARD_SELECTION = ("exonic", "exonic;splicing", "splicing")

# THIS IS THE STANDARD MAF CUTOFF, SHOULD BE CHANGED ACCORDING TO THE NEEDS
MAF_CUTOFF = 0.01

# GENOTYPE ANNOTATION ACCORDING TO ANNOVAR
GENOTYPE_HET = 'het'
GENOTYPE_HOM = 'hom'


# -----------------------------------------------------------------------------
#
#   Class:          Log
#
#   Description:    Class to collect output from methods filtering variants
#
#   Methods:        append_text(self, text): receive text and append it to the object
#                   get_text(self): returns text containing the appended log
#

class Log:
    def __init__(self):
        self.text = ""

    def append_text(self, text):
        self.text = self.text + "\n" + text

    def get_text(self):
        return self.text


# -----------------------------------------------------------------------------
#
#   Class:          Proband
#
#   Description:    Class to collect information regarding proband sample
#
#   Methods:        increment_variant_count(self): increment by 1 the number of variants associated to the proband
#                   def get_variant_count(self): returns the number of variants found for the proband
#

class Proband:
    def __init__(self):
        self.variant_count = 0

    def increment_variant_count(self):
        self.variant_count = self.variant_count + 1

    def get_variant_count(self):
        return self.variant_count


# -----------------------------------------------------------------------------
#
#   Class:          Columns
#
#   Description:    Class to collect information regarding ANNOVAR columns from each sample
#
#   Methods:        getters(self): returns the number of the column(s) containing the desired info
#                   setters(self, param): set the number of the column(s) containing the desired info according to parameter
#                   clean_cols_maf(self): erase content from cols_maf array
#                   append_cols_maf(self, col): append a new column number for cols_maf array
#

class Columns:
    def __init__(self):
        self.col_chromosome = COL_CHROMOSOME
        self.col_start_position = COL_START_POSITION
        self.col_ref_allele = COL_REF_ALLELE
        self.col_alt_allele = COL_ALT_ALLELE
        self.col_genotype = COL_GENOTYPE
        self.col_gene_location = COL_GENE_LOCATION
        self.col_gene_name = COL_GENE_NAME
        self.col_exon_func = COL_EXON_FUNC
        self.cols_maf = COLS_MAF

    def get_col_chromosome(self):
        return self.col_chromosome

    def get_col_start_position(self):
        return self.col_start_position

    def get_col_ref_allele(self):
        return self.col_ref_allele

    def get_col_alt_allele(self):
        return self.col_alt_allele

    def get_col_genotype(self):
        return self.col_genotype

    def get_col_gene_location(self):
        return self.col_gene_location

    def get_col_gene_name(self):
        return self.col_gene_name

    def get_col_exon_func(self):
        return self.col_exon_func

    def get_cols_maf(self):
        return self.cols_maf

    def set_col_chromosome(self, col_chromosome):
        self.col_chromosome = col_chromosome

    def set_col_start_position(self, col_start_position):
        self.col_start_position = col_start_position

    def set_col_ref_allele(self, col_ref_allele):
        self.col_ref_allele = col_ref_allele

    def set_col_alt_allele(self, col_alt_allele):
        self.col_alt_allele = col_alt_allele

    def set_col_genotype(self, col_genotype):
        self.col_genotype = col_genotype

    def set_col_gene_location(self, col_gene_location):
        self.col_gene_location = col_gene_location

    def set_col_gene_name(self, col_gene_name):
        self.col_gene_name = col_gene_name

    def set_col_exon_func(self, col_exon_func):
        self.col_exon_func = col_exon_func

    def set_cols_maf(self, cols_maf):
        self.cols_maf = cols_maf

    def clean_cols_maf(self):
        self.cols_maf = []

    def append_cols_maf(self, col):
        self.cols_maf.append(col)


# -----------------------------------------------------------------------------
#
#   Class:          Variant
#
#   Description:    Class to collect information regarding each variant for filtering steps
#
#   Methods:        getters(self): returns the content of the field
#                   setters(self, param): set the content of the field according to the parameter
#

class Variant:
    def __init__(self, chromosome, start_position, ref_allele, alt_allele, genotype, gene):
        self.chromosome = chromosome
        self.start_position = start_position
        self.ref_allele = ref_allele
        self.alt_allele = alt_allele
        self.genotype = genotype
        self.gene = gene

    def get_chromosome(self):
        return self.chromosome

    def get_start_position(self):
        return self.start_position

    def get_ref_allele(self):
        return self.ref_allele

    def get_alt_allele(self):
        return self.alt_allele

    def get_genotype(self):
        return self.genotype

    def get_gene(self):
        return self.gene

    def set_chromosome(self, chromosome):
        self.chromosome = chromosome

    def set_start_position(self, start_position):
        self.start_position = start_position

    def set_ref_allele(self, ref_allele):
        self.ref_allele = ref_allele

    def set_alt_allele(self, alt_allele):
        self.alt_allele = alt_allele

    def set_genotype(self, genotype):
        self.genotype = genotype

    def set_gene(self, gene):
        self.gene = gene


# -----------------------------------------------------------------------------
#
#   Class:          Sample
#
#   Description:    Class to collect information from PED file and header from ANNOVAR file
#
#   Methods:        getters(self): returns the content of the field
#                   setters(self, param): set the content of the field according to the parameter
#

class Sample:
    def __init__(self, family_member_id, family_id, sample_id, paternal_id, maternal_id, sex, affected_status,
                 file_path, header_cols):
        self.family_member_id = family_member_id
        self.family_id = family_id
        self.sample_id = sample_id
        self.paternal_id = paternal_id
        self.maternal_id = maternal_id
        self.sex = sex
        self.affected_status = affected_status
        self.file_path = file_path
        self.header_cols = header_cols
        self.header = ""

    def get_family_member_id(self):
        return self.family_member_id

    def get_family_id(self):
        return self.family_id

    def get_sample_id(self):
        return self.sample_id

    def get_paternal_id(self):
        return self.paternal_id

    def get_maternal_id(self):
        return self.maternal_id

    def get_sex(self):
        return self.sex

    def get_affected_status(self):
        return self.affected_status

    def get_file_path(self):
        return self.file_path

    def get_header_cols(self):
        return self.header_cols

    def get_header(self):
        return self.header

    def set_family_member_id(self, family_member_id):
        self.family_member_id = family_member_id

    def set_family_id(self, family_id):
        self.family_id = family_id

    def set_sample_id(self, sample_id):
        self.sample_id = sample_id

    def set_paternal_id(self, paternal_id):
        self.paternal_id = paternal_id

    def set_maternal_id(self, maternal_id):
        self.maternal_id = maternal_id

    def set_sex(self, sex):
        self.sex = sex

    def set_affected_status(self, affected_status):
        self.affected_status = affected_status

    def set_file_path(self, file_path):
        self.file_path = file_path

    def set_header_cols(self, header_cols):
        self.header_cols = header_cols

    def set_header(self, header):
        self.header = header


# -----------------------------------------------------------------------------
#
#   Method:         analyze_variants(variants, analysis_type, log_text):
#
#   Description:    Perform analysis filtering based on chosen analysis_type
#
#   Parameters:     variants[]          list of ANNOVAR lines to filtered
#                   analysis_type       analysis type according to the constants (int 0, 1, 2, 3, or 4)
#                   log_text            Log object to append log info from the method
#
#   Return:         analyzed_variants[] list of ANNOVAR lines filtered according the chosen analysis
#

def analyze_variants(variants, analysis_type, log_text):
    analyzed_variants = []

    if analysis_type == ANALYSIS_TYPE_AD_V:
        for var in variants:
            if var.split("\t")[COL_GENOTYPE] == GENOTYPE_HET:
                analyzed_variants.append(var)
        log_text.append_text('Including {0} in Genotype {1} variants -> {2} variants'.format(GENOTYPE_HET, str(len(variants)), str(len(analyzed_variants))))

    elif analysis_type == ANALYSIS_TYPE_AD_IM:
        return 0

    elif analysis_type == ANALYSIS_TYPE_AD_NM:
        return 0

    elif analysis_type == ANALYSIS_TYPE_AR_H:
        return 0

    elif analysis_type == ANALYSIS_TYPE_AR_CH:
        return 0

    return analyzed_variants


# -----------------------------------------------------------------------------
#
#   Method:         print_variants(variants):
#
#   Description:    print all variants from a variant list
#
#   Parameters:     variants[]          list of ANNOVAR lines
#
#   Return:         no returns
#

def print_variants(variants):
    for var in variants:
        print(var)


# -----------------------------------------------------------------------------
#
#   Method:         read_ped_file(file_ped, file_path):
#
#   Description:    create a list of Sample() objects from PED file and ANNOVAR paths
#
#   Parameters:     file_ped            ped file containing information of samples
#                   file_path[]         list of files containing the ANNOVAR variants
#
#   Return:         sample[]            list of Sample() objects
#

def read_ped_file(file_ped, file_path):
    # PED FILE SHOULD FOLLOW FAMILY_MEMBER ID ORDER, PROBAND FIRST, MOTHER SECOND, FATHER THIRD AND SO ON
    # PED FILE MUST CONTAIN PROBAND MOTHER AND FATHER
    with open(file_ped, 'r') as reader:
        family_member_id_count = 1

        samples = []
        c = Columns()
        for line in reader.readlines():
            family_id = line.split("\t")[0]
            sample_id = line.split("\t")[1]
            paternal_id = line.split("\t")[2]
            maternal_id = line.split("\t")[3]
            sex = line.split("\t")[4]
            affected_status = line.split("\t")[5]
            samples.append(
                Sample(family_member_id_count, family_id, sample_id, paternal_id, maternal_id, sex, affected_status,
                       file_path[family_member_id_count - 1], c))

        return samples


# -----------------------------------------------------------------------------
#
#   Method:         read_proband(sample, proband_variant_count):
#
#   Description:    read variants on ANNOVAR file from proband sample
#
#   Parameters:     sample              sample object containing proband info
#                   proband             proband object containing proband info
#
#   Return:         variants            list of variants
#

def read_proband(sample, proband):
    with open(sample.get_file_path(), 'r') as reader:

        header_meta = True
        header = True
        var_line = False
        variants = []

        for line in reader.readlines():

            if line[0] != "#":
                header_meta = False

            if header and header_meta is False:
                sample.set_header_cols(update_header_cols(line))
                sample.set_header(line)
                header = False

            if var_line and header is False:
                proband.increment_variant_count()
                variants.append(line[:-1])

            if var_line is False and header is False:
                var_line = True

        return variants


# -----------------------------------------------------------------------------
#
#   Method:         filter_maf(variants, cols, log_text):
#
#   Description:    filter variants based on MAF
#
#   Parameters:     variants        list of variants
#                   cols            col numbers where MAF from ANNOVAR file
#                   log_text        Log object to append log info from the method
#
#   Return:         variants_tmp    list of variants filtered variants
#

def filter_maf(variants, cols, log_text):
    variants_tmp = []

    for var in variants:
        print_var = True
        for n in cols:
            if format_maf(var.split("\t")[n]) > MAF_CUTOFF:
                print_var = False
                break
        if print_var:
            variants_tmp.append(var)

    log_text.append_text(
        'Excluding if value is greater than, {0} in selected MAF projects, {1} variants -> {2} variants'.format(
            MAF_CUTOFF, str(len(variants)), str(len(variants_tmp))))

    return variants_tmp


# -----------------------------------------------------------------------------
#
#   Method:         filter_refgene_gene_location(variants, log_text):
#
#   Description:    filter variants based on refGene gene location according to selection
#
#   Parameters:     variants        list of variants
#                   log_text        Log object to append log info from the method
#
#   Return:         variants_tmp    list of variants filtered variants
#

def filter_refgene_gene_location(variants, log_text):
    variants_tmp = []

    # SELECT THE DESIRED REFGENE GENE LOCATIONS FROM THE OPTIONS DESCRIBED ON
    # REFGENE_GENE_LOCATION_OPTIONS, STANDARD PHENODB SELECTION IMPLEMENTED
    refgene_gene_location_selection = REFGENE_GENE_LOCATION_STANDARD_SELECTION

    for var in variants:

        if check_refgene_gene_location(var, refgene_gene_location_selection):
            variants_tmp.append(var)

    log_text.append_text(
        'Including {0} in RefGeneLocation, {1} variants -> {2} variants'.format(refgene_gene_location_selection,
                                                                                str(len(variants)),
                                                                                str(len(variants_tmp))))

    return variants_tmp


# -----------------------------------------------------------------------------
#
#   Method:         check_refgene_gene_location(line, refgene_location_selection):
#
#   Description:    check if a variant is at desired location on refGene gene location
#
#   Parameters:     line                            ANNOVAR line with a variant
#                   refgene_location_selection      selection of desired locations
#
#   Return:         True              qualified variant
#                   False             not a qualified variant
#


def check_refgene_gene_location(line, refgene_location_selection):
    refgene_gene_location = line.split("\t")[COL_GENE_LOCATION]

    for location in refgene_location_selection:
        if refgene_gene_location == location:
            return True

    return False


# -----------------------------------------------------------------------------
#
#   Method:         filter_refgene_exon_function(variants, log_text):
#
#   Description:    filter synonymous variants except for exonic:splicing
#
#   Parameters:     variants        list of variants
#                   log_text        Log object to append log info from the method
#
#   Return:         variants_tmp    list of variants filtered variants
#

def filter_refgene_exon_function(variants, log_text):
    variants_tmp = []

    for var in variants:
        if check_refgene_exon_function(var):
            variants_tmp.append(var)

    log_text.append_text(
        'Excluding synonymous SNV in RefGeneExonFunction, except for exonic:splicing variants {0} variants -> {1} variants'.format(
            str(len(variants)), str(len(variants_tmp))))

    return variants_tmp


# -----------------------------------------------------------------------------
#
#   Method:         check_refgene_exon_function(line):
#
#   Description:    check if a synonymous is a qualified variant (synonymous & exonic:splicing)
#
#   Parameters:     line              ANNOVAR line with a variant
#
#   Return:         True              qualified variant
#                   False             not a qualified variant
#

def check_refgene_exon_function(line):
    refgene_gene_location = line.split("\t")[COL_GENE_LOCATION]
    refgene_exon_function = line.split("\t")[COL_EXON_FUNC]

    if refgene_exon_function == "synonymous SNV":
        if refgene_gene_location == "exonic;splicing":
            return True
        else:
            return False
    else:
        return True


# -----------------------------------------------------------------------------
#
#   Method:         format_maf(maf):
#
#   Description:    format MAF to become float for filtering comparison
#
#   Parameters:     maf             maf of a given variant in a given project

#
#   Return:         maf             maf formatted to float
#

def format_maf(maf):
    if maf == ".":
        return 0.0
    elif len(maf) == 0:
        return 0.0
    elif maf == "\t":
        return 0.0
    elif maf == "\n":
        return 0.0
    elif maf == ".\n":
        return 0.0
    else:
        return float(maf)


# -----------------------------------------------------------------------------
#
#   Method:         update_header_cols(header):
#
#   Description:    update the number of the columns for filtering steps
#
#   Parameters:     header              ANNOVAR line with header names
#
#   Return:         Columns()           object containing the position of ANNOVAR cols based on header names
#

def update_header_cols(header):
    fields = header.split("\t")
    columns = Columns()
    columns.clean_cols_maf()
    n = 0
    for field in fields:
        if field == "Chromosome" or field == "Chr":
            columns.set_col_chromosome(n)
        elif field == "StartPosition" or field == "Start":
            columns.set_col_start_position(n)
        elif field == "ReferenceAllele" or field == "Ref":
            columns.set_col_ref_allele(n)
        elif field == "AlternativeAllele" or field == "Alt":
            columns.set_col_alt_allele(n)
        elif field == "Genotype" or field == "GT":
            columns.set_col_genotype(n)
        elif field == "RefgeneGeneLocation":
            columns.set_col_gene_location(n)
        elif field == "RefgeneGeneName":
            columns.set_col_gene_name(n)
        elif field == "RefgeneExonFunction":
            columns.set_col_exon_func(n)
        elif field[:7] == "ExAC02_" or field[:7] == "ExAC03_" or field[:12] == "hg19_esp6500" or field[
                                                                                                 :19] == "Afalt_1000g2012Apr_" or field[
                                                                                                                                  :7] == "gnomAD_":
            columns.append_cols_maf(n)
        n = n + 1
    return columns


# -----------------------------------------------------------------------------
#
#   Method:         def main():
#
#   Description:    main method
#

def main():
    log_text = Log()

    # filepath = sys.argv[1]
    wd = "/Users/renan/Desktop/LAB_SOBREIRA/FILES"

# PED FILE FROM TEST DATASET
    file_ped = wd + "/TEST_SAMPLE2.ped"

# FILES FROM TEST DATASET
    file_path = [wd + "/NA18503@1054737211_MS_OnBait_ANNOVAR_REPORT.txt",
                 wd + "/NA18505@1054737132_MS_OnBait_ANNOVAR_REPORT.txt",
                 wd + "/NA18504@1054737158_MS_OnBait_ANNOVAR_REPORT.txt"]

    samples = read_ped_file(file_ped, file_path)

    proband = Proband()

    variants = read_proband(samples[0], proband)

    variants = filter_refgene_gene_location(variants, log_text)

    variants = filter_refgene_exon_function(variants, log_text)

    variants = filter_maf(variants, samples[0].get_header_cols().get_cols_maf(), log_text)

    variants = analyze_variants(variants, ANALYSIS_TYPE_AD_V, log_text)

    print(samples[0].get_header()[:-1])

    print_variants(variants)

    log_text.append_text('Initial proband count = {0} / Final count = {1}'.format(str(proband.get_variant_count()), str(len(variants))))

    print(log_text.get_text())


# -----------------------------------------------------------------------------
#

if __name__ == '__main__':
    main()

#
# -----------------------------------------------------------------------------
